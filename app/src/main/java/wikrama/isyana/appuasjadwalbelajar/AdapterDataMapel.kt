package wikrama.isyana.appuasjadwalbelajar

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

class AdapterDataMapel(val dataMapel: List<HashMap<String, String>>,val mapelActivity : FragmentMapel) :
    RecyclerView.Adapter<AdapterDataMapel.HolderDataMapel>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int ): AdapterDataMapel.HolderDataMapel {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_data_mapel,parent,false)
        return HolderDataMapel(v)
    }
    inner class HolderDataMapel(v: View): RecyclerView.ViewHolder(v){
        val txMapel = v.findViewById<TextView>(R.id.txMapelData)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)

    }

    override fun getItemCount(): Int {
        return dataMapel.size
    }

    override fun onBindViewHolder(holder: HolderDataMapel, position: Int) {
        val data = dataMapel.get(position)
        holder.txMapel.setText(data.get("nama_mapel"))
        if(position.rem(2)==0){
            holder.cLayout.setBackgroundColor(Color.rgb(230,245,240))
        }
        else{
            holder.cLayout.setBackgroundColor(Color.rgb(255,255,245))
        }

    }
}