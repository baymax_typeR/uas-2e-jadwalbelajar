package wikrama.isyana.appuasjadwalbelajar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

    class FragmentTutor : Fragment(){
    lateinit var thisParent : MainActivity
    lateinit var  v : View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v =  inflater.inflate(R.layout.frag_data_tutor,container,false)

        return v
    }
}