package wikrama.isyana.appuasjadwalbelajar

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import android.widget.MediaController
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.videoView

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    val RC_SUKSES : Int = 100
    var bg : String = "WHITE"
    var fdetail : Int = 20
    var isi : String = "Isi Detail"
    lateinit var pref : SharedPreferences
    val PREF_NAME = "Setiing"
    val FIELD_FONT_DETAIL = "Font_Size_Detail"
    val FIELD_BACK = "BACKGROUND"
    val FIELD_TEXT = "text"
    val DEF_FONT_DETAIL = fdetail
    val DEF_BACK = bg
    val DEF_ISI = isi

    lateinit var fragJadwal : FragmentJadwal
    lateinit var fragTutor : FragmentTutor
    lateinit var fragMapel : FragmentMapel
    //lateinit var fragVideo : FragmentVideo
    lateinit var  ft : FragmentTransaction

    val daftarVideo = intArrayOf(R.raw.mat_pgktdua, R.raw.mat_pgkttiga, R.raw.ipa_kalor, R.raw.ipa_listrikdinamis,
        R.raw.bing_partofspeech, R.raw.bing_tense)

    var posVideoSkrg = 0
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragJadwal = FragmentJadwal()
        fragTutor = FragmentTutor()
        fragMapel = FragmentMapel()
        pref =  getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txDetail1.setText(pref.getString(FIELD_TEXT,DEF_ISI))
        fdetail = pref.getInt(FIELD_FONT_DETAIL,DEF_FONT_DETAIL)
        bg = pref.getString(FIELD_BACK,DEF_BACK).toString()
        pemanggil()

        //fragVideo = FragmentVideo()

        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        mediaController.setPrevNextListeners(nextVid,prevVid)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
        videoSet(posVideoSkrg)
    }

    var nextVid = View.OnClickListener { v: View? ->
        if (posVideoSkrg<(daftarVideo.size-1)) posVideoSkrg++
        else posVideoSkrg = 0
        videoSet(posVideoSkrg)
    }

    var prevVid = View.OnClickListener { v: View? ->
        if (posVideoSkrg>0) posVideoSkrg--
        else posVideoSkrg = daftarVideo.size-1
        videoSet(posVideoSkrg)
    }

    fun videoSet(pos: Int){
        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+daftarVideo[pos]))
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemJadwal ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragJadwal).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemTutor ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragTutor).commit()
                frameLayout.setBackgroundColor(Color.argb(245,225,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemMapel ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragMapel).commit()
                frameLayout.setBackgroundColor(Color.argb(245,225,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemHome -> frameLayout.visibility = View.GONE
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_shared_preferences,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSetting->{
                var s = Intent(this,SettingActivity::class.java)
                s.putExtra("x",txDetail1.text.toString())
                startActivityForResult(s,RC_SUKSES)
                intent.putExtra("x",txDetail1.text.toString())
                startActivity(s)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == RC_SUKSES) {
                txDetail1.setText(data?.extras?.getString("isidetail"))
                isi = txDetail1.text.toString()
                bg = data?.extras?.getString("bgcolor").toString()
                fdetail = data?.extras?.getInt("tDetail").toString().toInt()
                //Toast.makeText(this,"Result", Toast.LENGTH_SHORT).show()
                simpan()
                pemanggil()
            }
        }
    }
    fun simpan(){
        pref = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        val prefEdit = pref.edit()
        prefEdit.putInt(FIELD_FONT_DETAIL,fdetail)
        prefEdit.putString(FIELD_TEXT,isi)
        prefEdit.putString(FIELD_BACK,bg)
        prefEdit.commit()
        //Toast.makeText(this,"Simpan", Toast.LENGTH_SHORT).show()
    }
    fun pemanggil(){
        Font_Detail()
        backgroundmain()
    }
    fun Font_Detail(){
        txDetail1.textSize = fdetail.toFloat()
    }
    fun backgroundmain(){
        if (bg=="Blue"){
            constraintlayout.setBackgroundColor(Color.BLUE)
        }
        else if(bg=="Yellow"){
            constraintlayout.setBackgroundColor(Color.YELLOW)
        }
        else if(bg=="Green"){
            constraintlayout.setBackgroundColor(Color.GREEN)
        }
        else if(bg=="White"){
            constraintlayout.setBackgroundColor(Color.BLACK)
        }
    }
}
