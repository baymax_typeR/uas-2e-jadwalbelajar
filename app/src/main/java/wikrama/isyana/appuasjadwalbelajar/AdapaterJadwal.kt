package wikrama.isyana.appuasjadwalbelajar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text

class AdapaterJadwal(val dataJadwal : List<HashMap<String,String>>,val jadwal: FragmentJadwal) : RecyclerView.Adapter<AdapaterJadwal.HolderDataJadwal>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int ): AdapaterJadwal.HolderDataJadwal {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_data_jadwal,parent,false)
        return HolderDataJadwal(v)
    }

    override fun getItemCount(): Int {
        return dataJadwal.size
    }

    inner class HolderDataJadwal(v: View): RecyclerView.ViewHolder(v){
        val txNamaMapel = v.findViewById<TextView>(R.id.txtNmMapel)
        val txNmTutor = v.findViewById<TextView>(R.id.txtNamaTutor)
        val txTanggal = v.findViewById<TextView>(R.id.txTgl)
        val txJam = v.findViewById<TextView>(R.id.txtJam)
    }

    override fun onBindViewHolder(holder: HolderDataJadwal, position: Int) {

    }
}