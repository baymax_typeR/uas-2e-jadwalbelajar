package wikrama.isyana.appuasjadwalbelajar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray

class FragmentJadwal : Fragment(), View.OnClickListener {

    var daftarJadwal = mutableListOf<HashMap<String, String>>()
    var daftarTutor = mutableListOf<HashMap<String, String>>()
    var daftarMapel = mutableListOf<HashMap<String, String>>()
    val uri = "http://192.168.12/dashboard/uas-2e-jadwalbelajar-web/web/show_data_tutor.php"
    val uri1 = ""
    val uri2 = ""
    lateinit var thisParent: MainActivity
    lateinit var v: View
    lateinit var jadwalAdapter: AdapaterJadwal

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_jadwal, container, false)

        return v
    }

    override fun onClick(v: View?) {

    }

    fun getNamaTutor() {
        val request = object : StringRequest(Request.Method.POST, uri,
            Response.Listener { response ->
                daftarTutor.clear()
                Toast.makeText(thisParent, "koneksi ke server (data mhs)", Toast.LENGTH_SHORT)
                    .show()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String, String>()
                    mhs.put("nim", jsonObject.getString("nim"))
                    mhs.put("nama", jsonObject.getString("nama"))
                    mhs.put("nama_prodi", jsonObject.getString("nama_prodi"))
                    mhs.put("url", jsonObject.getString("url"))
                    mhs.put("ttl", jsonObject.getString("ttl"))
                    mhs.put("jk", jsonObject.getString("jk"))
                    mhs.put("alamat", jsonObject.getString("alamat"))
                    daftarTutor.add(mhs)
                }
                jadwalAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(
                    thisParent,
                    "Terjadi kesalahan koneksi ke server",
                    Toast.LENGTH_SHORT
                ).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()

                return hm
            }
        }
    }


    }
