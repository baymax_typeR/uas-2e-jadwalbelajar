package wikrama.isyana.appuasjadwalbelajar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray

class FragmentMapel : Fragment() {
    lateinit var thisParent: MainActivity
    lateinit var v: View
    lateinit var mapelAdapter: AdapterDataMapel
    var daftarMapel = mutableListOf<HashMap<String, String>>()
    val uri = "http://192.168.1.12/dashboard/uas-2e-jadwalbelajar-web/web/show_data_mapel.php"
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_mapel, container, false)

        return v
    }

    fun showDataMapel(namaMapel: String) {
        val request = object : StringRequest(Request.Method.POST, uri,
            Response.Listener { response ->
                daftarMapel.clear()
                Toast.makeText(thisParent, "koneksi ke server (data mhs)", Toast.LENGTH_SHORT)
                    .show()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mapel = HashMap<String, String>()
                    mapel.put("nama_mapel", jsonObject.getString("nama_mapel"))
                    daftarMapel.add(mapel)
                }
                mapelAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(
                    thisParent,
                    "Terjadi kesalahan koneksi ke server",
                    Toast.LENGTH_SHORT
                ).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("nama_mapel", namaMapel)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataMapel("")
    }
}