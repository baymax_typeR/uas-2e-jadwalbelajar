package wikrama.isyana.appuasjadwalbelajar

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener{

    var arrayWarna = arrayOf("Blue","Yellow","Green","Black","White")
    lateinit var adapterSpin : ArrayAdapter<String>
    var bg : String = ""
    var x : String = ""
    var fdetail : Int = 0
    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                fdetail = progress.toFloat().toInt()
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        adapterSpin = ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayWarna)
        spWarnaBg.adapter = adapterSpin
        btnSimpanSetting.setOnClickListener(this)
        var paket : Bundle? = intent.extras
        editText.setText(paket?.getString("x"))
        seekBar.setOnSeekBarChangeListener(onSeek)
        spWarnaBg.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext,"Tidak ada yang dipilih",Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Toast.makeText(baseContext,adapterSpin.getItem(position),Toast.LENGTH_SHORT).show()
                bg = adapterSpin.getItem(spWarnaBg.selectedItemPosition).toString()
            }

        }

    }

    override fun onClick(v: View?) {
        x = editText.text.toString()
        finish()
        //Toast.makeText(this,"Perubahan Telah Disimpan",Toast.LENGTH_SHORT).show()
    }

    override fun finish() {
        var intent = Intent()
        intent.putExtra("bgcolor",bg)
        intent.putExtra("isidetail",x)
        intent.putExtra("tDetail",fdetail)
        setResult(Activity.RESULT_OK,intent)
        super.finish()
    }

}